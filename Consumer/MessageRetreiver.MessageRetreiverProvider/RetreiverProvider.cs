﻿using DataTypes;
using MessageRetreiver.MessageRetreiver;
using System;
using System.Threading.Tasks;

namespace MessageRetreiver.MessageRetreiverProvider
{
    public class RetreiverProvider : IRetreiverProvider
    {
        readonly IRetreiver _retreiver;

        public RetreiverProvider(IRetreiver retreiver)
        {
            _retreiver = retreiver;
            _retreiver.MessageRetreived += _retreiver_MessageRetreived;
        }

        public delegate Task MessageRetreivedEventHandler(object sender, EventArgs eventArgs);

        public event MessageRetreivedEventHandler MessageRetreived;

        Task _retreiver_MessageRetreived(object sender, EventArgs eventArgs)
        {
            return MessageRetreived?.Invoke(sender, eventArgs);
        }

        public void Start()
        {
            _retreiver.InitializeRetreiver(MessageRetreiverConfiguration.HostAdress, MessageRetreiverConfiguration.QueueName, MessageRetreiverConfiguration.UserName, MessageRetreiverConfiguration.UserPassword);
            _retreiver.StartSender();
            Console.ReadKey();
        }

        public void Stop()
        {
            _retreiver.StopSender();
        }
    }
}
