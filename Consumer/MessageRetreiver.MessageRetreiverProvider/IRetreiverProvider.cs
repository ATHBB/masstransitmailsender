﻿namespace MessageRetreiver.MessageRetreiverProvider
{
    public interface IRetreiverProvider
    {
        event RetreiverProvider.MessageRetreivedEventHandler MessageRetreived;

        void Start();
        void Stop();
    }
}