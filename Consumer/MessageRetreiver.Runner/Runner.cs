﻿using DataTypes;
using Logger;
using MessageRetreiver.Runner.Service;
using Ninject;
using System.Configuration;
using System.Reflection;

namespace MessageRetreiver.Runner
{
    class Runner
    {
        static void Main()
        {
            GetConfigurationValues();
            var kernel = new StandardKernel();
            kernel.Load(Assembly.GetExecutingAssembly());
            kernel.Get<ILogger>().InitializeLogger();
            kernel.Get<IService>().Initialize();
        }

        static void GetConfigurationValues()
        {
            MessageRetreiverConfiguration.MailTemplateName = ConfigurationManager.AppSettings["mailTemplate"];
            MessageRetreiverConfiguration.HostAdress = ConfigurationManager.AppSettings["hostAdress"];
            MessageRetreiverConfiguration.QueueName = ConfigurationManager.AppSettings["queueName"];
            MessageRetreiverConfiguration.ServiceName = ConfigurationManager.AppSettings["serviceName"];
            MessageRetreiverConfiguration.UserName = ConfigurationManager.AppSettings["userName"];
            MessageRetreiverConfiguration.UserPassword = ConfigurationManager.AppSettings["userPassword"];
        }
    }
}
