﻿using Logger;
using MessageRetreiver.MailSender;
using MessageRetreiver.MailSender.Sender;
using MessageRetreiver.MessageRetreiver;
using MessageRetreiver.MessageRetreiverProvider;
using MessageRetreiver.Runner.Service;
using Ninject.Modules;

namespace MessageRetreiver.Runner
{
    public class NinjectConfig : NinjectModule
    {
        public override void Load()
        {
            Bind<ILogger>().To<Logger.Logger>().InSingletonScope();
            Bind<IMailSender>().To<MailSender.Sender.MailSender>().InSingletonScope();
            Bind<IMainWorker>().To<MainWorker>().InSingletonScope();
            Bind<IRetreiverProvider>().To<RetreiverProvider>().InSingletonScope();
            Bind<IRetreiver>().To<Retreiver>().InSingletonScope();
            Bind<IService>().To<Service.Service>().InSingletonScope();
        }
    }
}
