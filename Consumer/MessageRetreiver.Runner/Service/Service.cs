﻿using DataTypes;
using Logger;
using MessageRetreiver.MailSender;
using MessageRetreiver.MailSender.Sender;
using MessageRetreiver.MessageRetreiverProvider;
using Topshelf;

namespace MessageRetreiver.Runner.Service
{
    public class Service : IService
    {
        readonly ILogger _logger;
        readonly IMailSender _mailSender;
        readonly IRetreiverProvider _retreiveProvider;
        public Service(ILogger logger, IMailSender mailSender, IRetreiverProvider retreiveProvider)
        {
            _logger = logger;
            _mailSender = mailSender;
            _retreiveProvider = retreiveProvider;
        }
        public void Initialize()
        {
            HostFactory.Run(configure =>
            {
                configure.Service<IMainWorker>(service =>
                {
                    service.ConstructUsing(() => new MainWorker(_retreiveProvider, _mailSender, _logger));
                    service.WhenStarted(s => s.Start());
                    service.WhenStopped(s => s.Stop());
                });
                configure.RunAsLocalSystem();
                configure.SetServiceName(MessageRetreiverConfiguration.ServiceName);
                configure.SetDisplayName(MessageRetreiverConfiguration.ServiceName);
                configure.SetDescription(MessageRetreiverConfiguration.ServiceName);
            });
        }
    }
}
