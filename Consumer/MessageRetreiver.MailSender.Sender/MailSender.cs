﻿using DataTypes;
using FluentMailer.Factory;
using FluentMailer.Interfaces;
using Logger;
using System;

namespace MessageRetreiver.MailSender.Sender
{
    public class MailSender : IMailSender
    {
        readonly IFluentMailer _FluentMailer;
        readonly ILogger _logger;

        public MailSender(ILogger logger)
        {
            _FluentMailer = FluentMailerFactory.Create();
            _logger = logger;
        }

        public void SendMail(PersonMessage data, string mailBodyTemplate)
        {
            try
            {
                _FluentMailer.CreateMessage()
                        .WithView(mailBodyTemplate, data)
                       .WithReceiver(data.Email)
                       .WithSubject("Zostań konsultantem AVON!!")
                       .Send();
            }
            catch (Exception ex)
            {
                _logger.AddLogError($"An error occurred while sending mail to {data.Email}", ex);
            }

        }
    }
}
