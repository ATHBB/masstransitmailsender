﻿using DataTypes;

namespace MessageRetreiver.MailSender.Sender
{
    public interface IMailSender
    {
        void SendMail(PersonMessage data, string mailBodyTemplate);
    }
}