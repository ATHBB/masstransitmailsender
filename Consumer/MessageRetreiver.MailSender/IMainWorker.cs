﻿namespace MessageRetreiver.MailSender
{
    public interface IMainWorker
    {
        void Start();
        void Stop();
    }
}