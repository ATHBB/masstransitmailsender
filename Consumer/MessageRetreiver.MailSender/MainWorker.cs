﻿using DataTypes;
using Logger;
using MessageRetreiver.MailSender.Sender;
using MessageRetreiver.MessageRetreiverProvider;
using System;
using System.Threading.Tasks;

namespace MessageRetreiver.MailSender
{
    public class MainWorker : IMainWorker
    {
        readonly IRetreiverProvider _retreiverProvider;
        readonly IMailSender _mailSender;
        readonly ILogger _logger;

        public MainWorker(IRetreiverProvider retreiverProvider, IMailSender mailSender, ILogger logger)
        {
            _mailSender = mailSender;
            _logger = logger;
            _retreiverProvider = retreiverProvider;
            _retreiverProvider.MessageRetreived += _retreiverProvider_MessageRetreived;
        }

        Task _retreiverProvider_MessageRetreived(object sender, EventArgs eventArgs)
        {
            _logger.AddLogEntry($"Sending mail to {((MessageEventArgs)eventArgs).Message.Email} ...");
            _mailSender.SendMail(((MessageEventArgs)eventArgs).Message, MessageRetreiverConfiguration.MailTemplateName);
            return Task.FromResult(0);
        }

        public void Start()
        {
            _logger.AddLogEntry("Service starting...");
            _retreiverProvider.Start();

        }

        public void Stop()
        {
            _logger.AddLogEntry("Service stoping...");
            _retreiverProvider.Stop();
        }
    }
}
