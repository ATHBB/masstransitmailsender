﻿using DataTypes;
using MassTransit;
using System;
using System.Threading.Tasks;

namespace MessageRetreiver.MessageRetreiver
{
    public class Retreiver : IRetreiver
    {
        IBusControl bus;

        public delegate Task MessageRetreivedEventHandler(object sender, EventArgs eventArgs);

        public event MessageRetreivedEventHandler MessageRetreived;

        public void InitializeRetreiver(string hostUri, string queueName, string userName, string userPassword)
        {
            bus = Bus.Factory.CreateUsingRabbitMq(cfg =>
            {
                var host = cfg.Host(new Uri(hostUri), hostcfg =>
                {
                    hostcfg.Username(userName);
                    hostcfg.Password(userPassword);
                });
                cfg.ReceiveEndpoint(queueName, endpointcfg =>
                {
                    endpointcfg.UseRateLimit(100, TimeSpan.FromSeconds(60));
                    endpointcfg.Handler<PersonMessage>(x => MessageRetreived?.Invoke(this, new MessageEventArgs { Message = x.Message }));
                });
            });

        }

        public void StartSender()
        {
            bus.Start();
        }

        public void StopSender()
        {
            bus.Stop();
        }
    }
}
