﻿namespace MessageRetreiver.MessageRetreiver
{
    public interface IRetreiver
    {
        event Retreiver.MessageRetreivedEventHandler MessageRetreived;

        void InitializeRetreiver(string uriString, string queueName, string userName, string userPassword);
        void StartSender();
        void StopSender();
    }
}