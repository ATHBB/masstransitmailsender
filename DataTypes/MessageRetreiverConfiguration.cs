﻿namespace DataTypes
{
    public static class MessageRetreiverConfiguration
    {
        public static string ServiceName { get; set; }
        public static string MailTemplateName { get; set; }
        public static string HostAdress { get; set; }
        public static string QueueName { get; set; }
        public static string UserName { get; set; }
        public static string UserPassword { get; set; }
    }
}
