﻿namespace DataTypes
{
    public static class MessageSenderConfiguration
    {
        public static string DataFilePath { get; set; }
        public static string HostAdress { get; set; }
        public static string QueueAdress { get; set; }
        public static string ServiceName { get; set; }
        public static string UserName { get; set; }
        public static string UserPassword { get; set; }
    }
}
