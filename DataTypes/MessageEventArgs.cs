﻿using System;

namespace DataTypes
{
    public class MessageEventArgs : EventArgs
    {
        public PersonMessage Message { get; set; }
    }
}
