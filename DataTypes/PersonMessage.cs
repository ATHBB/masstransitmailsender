﻿namespace DataTypes
{
    public class PersonMessage
    {
        public string Name { get; set; }
        public string SecondName { get; set; }
        public string Email { get; set; }
    }
}
