﻿using DataTypes;
using Logger;
using MessageSender.DataParser;
using MessageSender.MessageSenderProvider;
using System;

namespace MessageSender
{
    public class MainWorker : IMainWorker
    {
        readonly IDataParser _dataParser;
        readonly ISenderProvider _senderProvider;
        readonly ILogger _logger;

        public MainWorker(ILogger logger, ISenderProvider senderProvider, IDataParser dataParser)
        {
            _logger = logger;
            _senderProvider = senderProvider;
            _dataParser = dataParser;
            _dataParser.DataRowReaded += _dataParser_DataRowReaded;
        }

        void _dataParser_DataRowReaded(object sender, EventArgs eventArgs)
        {
            _senderProvider.SendMessage(((MessageEventArgs)eventArgs).Message);
        }

        public void Start()
        {
            _logger.AddLogEntry("Service starting...");
            _senderProvider.InitializeSender();
            _senderProvider.StartSender();
            _dataParser.ReadFile(MessageSenderConfiguration.DataFilePath, false, ";");
        }

        public void Stop()
        {
            _logger.AddLogEntry("Service stoping...");
            _senderProvider.StopSender();
        }
    }
}
