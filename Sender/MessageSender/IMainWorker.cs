﻿namespace MessageSender
{
    public interface IMainWorker
    {
        void Start();
        void Stop();
    }
}