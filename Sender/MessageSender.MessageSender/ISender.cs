﻿namespace MessageSender.MessageSender
{
    public interface ISender
    {
        void CreateEndpoint(string stringUri);
        void InitializeSender(string uriString, string userName, string userPassword);
        void SendMessage<T>(T message) where T : class;
        void StartSender();
        void StopSender();
    }
}