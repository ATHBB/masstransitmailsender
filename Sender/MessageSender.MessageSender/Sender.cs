﻿using Logger;
using MassTransit;
using System;

namespace MessageSender.MessageSender
{
    public class Sender : ISender
    {
        IBusControl bus;
        ISendEndpoint endPoint;
        readonly ILogger _logger;

        public Sender(ILogger logger)
        {
            _logger = logger;
        }

        public void CreateEndpoint(string stringUri) => endPoint = bus.GetSendEndpoint(new Uri(stringUri)).Result;

        public void InitializeSender(string uriString, string userName, string userPassword)
        {
            bus = Bus.Factory.CreateUsingRabbitMq(cfg =>
            {
                var host = cfg.Host(new Uri(uriString), hostcfg =>
                {
                    hostcfg.Username(userName);
                    hostcfg.Password(userPassword);
                });
            });

        }

        public void SendMessage<T>(T message) where T : class
        {
            try
            {
                endPoint.Send(message);
                _logger.AddLogEntry("Message was sended");
            }
            catch (Exception ex)
            {
                _logger.AddLogError("An error occured while sending message", ex);
            }
        }

        public void StartSender()
        {
            _logger.AddLogEntry("Bus started");
            bus.Start();
        }

        public void StopSender()
        {
            _logger.AddLogEntry("Bus stopped");
            bus.Stop();
        }
    }
}
