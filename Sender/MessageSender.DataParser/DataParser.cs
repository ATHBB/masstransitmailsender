﻿using CsvHelper;
using CsvHelper.Configuration;
using DataTypes;
using Logger;
using System;
using System.IO;

namespace MessageSender.DataParser
{
    public class DataParser : IDataParser
    {
        readonly ILogger _logger;
        public DataParser(ILogger logger)
        {
            _logger = logger;
        }

        public delegate void DataRowReadedEventHandler(object sender, EventArgs eventArgs);

        public event DataRowReadedEventHandler DataRowReaded;

        public void ReadFile(string filePath, bool hasHeaderRecord, string delimeter)
        {
            if (File.Exists(filePath))
            {
                try
                {
                    using (var sr = new StreamReader(filePath))
                    {
                        var csv = new CsvReader(sr, new CsvConfiguration
                        {
                            HasHeaderRecord = hasHeaderRecord,
                            Delimiter = delimeter
                        });
                        while (csv.Read())
                        {
                            DataRowReaded?.Invoke(this, new MessageEventArgs { Message = csv.GetRecord<PersonMessage>() });
                        }
                    }
                }
                catch (Exception ex)
                {
                    _logger.AddLogError("An error occurred while parsing file with data", ex);
                }

            }
            else
                _logger.AddLogEntry("File with data doesn't exist");
        }
    }
}
