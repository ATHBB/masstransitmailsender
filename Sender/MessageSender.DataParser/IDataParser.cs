﻿namespace MessageSender.DataParser
{
    public interface IDataParser
    {
        event DataParser.DataRowReadedEventHandler DataRowReaded;
        void ReadFile(string filePath, bool hasHeaderRecord, string delimeter);
    }
}