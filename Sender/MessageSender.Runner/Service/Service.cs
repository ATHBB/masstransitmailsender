﻿using DataTypes;
using Logger;
using MessageSender.DataParser;
using MessageSender.MessageSenderProvider;
using System;
using Topshelf;

namespace MessageSender.Runner.Service
{
    public class Service : IService
    {
        readonly ILogger _logger;
        readonly ISenderProvider _senderProvider;
        readonly IDataParser _dataParser;

        public Service(ILogger logger, ISenderProvider senderProvider, IDataParser dataParser)
        {
            _logger = logger;
            _senderProvider = senderProvider;
            _dataParser = dataParser;
        }

        public void Initialize()
        {
            try
            {
                HostFactory.Run(configure =>
                {
                    configure.Service<IMainWorker>(service =>
                    {
                        service.ConstructUsing(() => new MainWorker(_logger, _senderProvider, _dataParser));
                        service.WhenStarted(s => s.Start());
                        service.WhenStopped(s => s.Stop());
                    });
                    configure.RunAsLocalSystem();
                    configure.SetServiceName(MessageSenderConfiguration.ServiceName);
                    configure.SetDisplayName(MessageSenderConfiguration.ServiceName);
                    configure.SetDescription(MessageSenderConfiguration.ServiceName);
                });
            }
            catch (Exception ex)
            {
                _logger.AddLogError("An error occurred while creating service", ex);
            }
        }
    }
}
