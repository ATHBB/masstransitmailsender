﻿using DataTypes;
using Logger;
using MessageSender.Runner.Service;
using Ninject;
using System.Configuration;
using System.Reflection;

namespace MessageSender.Runner
{
    class Program
    {
        static void Main()
        {
            var kernel = new StandardKernel();
            kernel.Load(Assembly.GetExecutingAssembly());
            GetConfigurationValues();
            kernel.Get<ILogger>().InitializeLogger();
            kernel.Get<IService>().Initialize();
        }

        static void GetConfigurationValues()
        {
            MessageSenderConfiguration.DataFilePath = ConfigurationManager.AppSettings["dataFilePath"];
            MessageSenderConfiguration.HostAdress = ConfigurationManager.AppSettings["hostAdress"];
            MessageSenderConfiguration.QueueAdress = ConfigurationManager.AppSettings["queueAdress"];
            MessageSenderConfiguration.ServiceName = ConfigurationManager.AppSettings["serviceName"];
            MessageSenderConfiguration.UserName = ConfigurationManager.AppSettings["userName"];
            MessageSenderConfiguration.UserPassword = ConfigurationManager.AppSettings["userPassword"];
        }
    }
}
