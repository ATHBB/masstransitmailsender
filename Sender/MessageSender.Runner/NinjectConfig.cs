﻿using Logger;
using MessageSender.DataParser;
using MessageSender.MessageSender;
using MessageSender.MessageSenderProvider;
using MessageSender.Runner.Service;
using Ninject.Modules;

namespace MessageSender.Runner
{
    public class NinjectConfig : NinjectModule
    {
        public override void Load()
        {
            Bind<ILogger>().To<Logger.Logger>().InSingletonScope();
            Bind<IMainWorker>().To<MainWorker>().InSingletonScope();
            Bind<IDataParser>().To<DataParser.DataParser>().InSingletonScope();
            Bind<ISender>().To<Sender>().InSingletonScope();
            Bind<ISenderProvider>().To<SenderProvider>().InSingletonScope();
            Bind<IService>().To<Service.Service>().InSingletonScope();
        }
    }
}
