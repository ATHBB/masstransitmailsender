﻿using DataTypes;

namespace MessageSender.MessageSenderProvider
{
    public interface ISenderProvider
    {
        void InitializeSender();
        void SendMessage(PersonMessage message);
        void StartSender();
        void StopSender();
    }
}