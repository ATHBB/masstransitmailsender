﻿using DataTypes;
using MessageSender.MessageSender;

namespace MessageSender.MessageSenderProvider
{
    public class SenderProvider : ISenderProvider
    {
        readonly ISender _messageSender;

        public SenderProvider(ISender messageSender)
        {
            _messageSender = messageSender;
        }

        public void InitializeSender()
        {
            _messageSender.InitializeSender(MessageSenderConfiguration.HostAdress, MessageSenderConfiguration.UserName, MessageSenderConfiguration.UserPassword);
            _messageSender.CreateEndpoint(MessageSenderConfiguration.QueueAdress);
        }

        public void SendMessage(PersonMessage message) => _messageSender.SendMessage(message);

        public void StartSender() => _messageSender.StartSender();

        public void StopSender() => _messageSender.StopSender();
    }
}
