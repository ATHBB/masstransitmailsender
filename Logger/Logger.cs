﻿using Serilog;
using System;

namespace Logger
{
    public class Logger : ILogger
    {
        public void InitializeLogger()
        {
            Log.Logger = new LoggerConfiguration()
                    .WriteTo.LiterateConsole()
                    .WriteTo.RollingFile($"log-{DateTime.Now}.txt")
                    .CreateLogger();
        }

        public void AddLogEntry(string text)
        {
            Log.Information(text);
        }

        public void AddLogError(string text, Exception ex)
        {
            Log.Error(ex, text);
        }
    }
}
