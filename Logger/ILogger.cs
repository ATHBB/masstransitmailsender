﻿using System;

namespace Logger
{
    public interface ILogger
    {
        void AddLogEntry(string text);
        void AddLogError(string text, Exception ex);
        void InitializeLogger();
    }
}